<?php
/*
Plugin Name: Katana
Description: Abstraction of site functionality from the theme. Pairs with Ronin.
Version: 0.1
Author: Stephen Wright
Author URI: http://kryptonitelabs.com
*/

remove_action('wp_head', 'rsd_link'); // Remove support for desktop blogging software (remote blogging). Also disables trackbacks/pingbacks.
remove_action('wp_head', 'wlwmanifest_link'); // Remove support for Windows Live Writer (enables WLW tagging)
remove_action('wp_head', 'wp_generator'); // Remove generator from wp_head (easy security fix)
remove_action('wp_head', 'feed_links', 2);
remove_action('wp_head', 'feed_links_extra', 3);
remove_action('wp_head', 'index_rel_link');
remove_action('wp_head', 'parent_post_rel_link', 10, 0);
remove_action('wp_head', 'start_post_rel_link', 10, 0);
remove_action('wp_head', 'adjacent_posts_rel_link_wp_head', 10, 0);
remove_action('wp_head', 'wp_shortlink_wp_head', 10, 0);
remove_action('wp_head', 'noindex', 1);

// remove WordPress version from RSS feeds **Credit to Roots theme (https://github.com/retlehs/roots)
function katana_no_generator() { return ''; }
add_filter('the_generator', 'katana_no_generator');

function katana_setup() {
	// Enables Post Thumbnails
	add_theme_support('post-thumbnails');
	// Enables Post Formats
	add_theme_support('post-formats', array('aside', 'gallery', 'link', 'image', 'quote', 'status', 'video', 'audio', 'chat'));
}

add_action('after_setup_theme', 'katana_setup');

if (!is_admin()) {
  wp_deregister_script('l10n');
}

// remove CSS from recent comments widget **Credit to Roots theme (https://github.com/retlehs/roots)
function katana_remove_recent_comments_style() {
  global $wp_widget_factory;
  if (isset($wp_widget_factory->widgets['WP_Widget_Recent_Comments'])) {
    remove_action('wp_head', array($wp_widget_factory->widgets['WP_Widget_Recent_Comments'], 'recent_comments_style'));
  }
}

add_action('wp_head', 'katana_remove_recent_comments_style', 1);

// remove CSS from gallery **Credit to Roots theme (https://github.com/retlehs/roots)
function katana_gallery_style($css) {
  return preg_replace("!<style type='text/css'>(.*?)</style>!s", '', $css);
}

add_filter('gallery_style', 'katana_gallery_style');
//==Clean URLs (http://yourdomain.com/wp-content => /wp-content) **Credit to Roots theme (https://github.com/retlehs/roots)
function katana_root_relative_url($input) {
  $output = preg_replace_callback(
    '!(https?://[^/|"]+)([^"]+)?!',
    create_function(
      '$matches',
      // if full URL is site_url, return a slash for relative root
      'if (isset($matches[0]) && $matches[0] === site_url()) { return "/";' .
      // if domain is equal to site_url, then make URL relative
      '} elseif (isset($matches[0]) && strpos($matches[0], site_url()) !== false) { return $matches[2];' .
      // if domain is not equal to site_url, do not make external link relative
      '} else { return $matches[0]; };'
    ),
    $input
  );
  return $output;
}

// workaround to remove the duplicate subfolder in the src of JS/CSS tags
// example: /subfolder/subfolder/css/style.css
function katana_fix_duplicate_subfolder_urls($input) {
  $output = katana_root_relative_url($input);
  preg_match_all('!([^/]+)/([^/]+)!', $output, $matches);
  if (isset($matches[1]) && isset($matches[2])) {
    if ($matches[1][0] === $matches[2][0]) {
      $output = substr($output, strlen($matches[1][0]) + 1);
    }
  }
  return $output;
}

if (!is_admin() && !in_array($GLOBALS['pagenow'], array('wp-login.php', 'wp-register.php'))) {
  add_filter('bloginfo_url', 'katana_root_relative_url');
  add_filter('theme_root_uri', 'katana_root_relative_url');
  add_filter('stylesheet_directory_uri', 'katana_root_relative_url');
  add_filter('template_directory_uri', 'katana_root_relative_url');
  add_filter('script_loader_src', 'katana_root_relative_url');
  add_filter('style_loader_src', 'katana_root_relative_url');
  add_filter('plugins_url', 'katana_root_relative_url');
  add_filter('the_permalink', 'katana_root_relative_url');
  add_filter('wp_list_pages', 'katana_root_relative_url');
  add_filter('wp_list_categories', 'katana_root_relative_url');
  add_filter('wp_nav_menu', 'katana_root_relative_url');
  add_filter('the_content_more_link', 'katana_root_relative_url');
  add_filter('the_tags', 'katana_root_relative_url');
  add_filter('get_pagenum_link', 'katana_root_relative_url');
  add_filter('get_comment_link', 'katana_root_relative_url');
  add_filter('month_link', 'katana_root_relative_url');
  add_filter('day_link', 'katana_root_relative_url');
  add_filter('year_link', 'katana_root_relative_url');
  add_filter('tag_link', 'katana_root_relative_url');
  add_filter('the_author_posts_link', 'katana_root_relative_url');
}

// remove root relative URLs on any attachments in the feed
function katana_root_relative_attachment_urls() {
  if (!is_feed()) {
    add_filter('wp_get_attachment_url', 'katana_root_relative_url');
    add_filter('wp_get_attachment_link', 'katana_root_relative_url');
  }
}

add_action('pre_get_posts', 'katana_root_relative_attachment_urls');

//==Register Scripts and Styles
function katana_assets(){
	global $wp_scripts;
  $katana_path = plugins_url( 'katana' );
  //==Styles
  wp_register_style( 'katana_base' , $katana_path . '/css/base.css', false, '1.0', 'all' );
  //==Scripts
  wp_register_script( 'jquery', '', '', '', true ); // Loads default WP jQuery in the footer w/o deregistering, unless another script w/ prereq loads in the header
  wp_register_script( 'jquery-noCDN', true, array( 'jquery' ), '0.1', true );
  wp_register_script( 'comment-reply', '', '', '', true ); // ::Feature:: Moves comment-reply.js to load in the footer. See http://core.trac.wordpress.org/ticket/12641
  wp_register_script( 'modernizr', $katana_path . '/js/modernizr.min.js', false, '2.5.2', false ); // Only script allowed in the header to load feature support classes ASAP (html.no-js => html.js...etc).
  //==Enqueues
  wp_enqueue_style( 'katana_base' );
  wp_enqueue_script( 'modernizr' );
  wp_enqueue_script( 'jquery-noCDN' ); // Not liking this at all. Auto-Loads jQuery. Also will not jump to head if jQuery is a dep of a header script.
  //==jQuery Magic
  $wp_scripts->add_data( 'jquery-noCDN', 'data', "window.jQuery || document.write('<script src='".$wp_scripts->registered['jquery']->src."'><\/script>');" );
  $wp_scripts->add_data( 'jquery', 'cdn', '//ajax.googleapis.com/ajax/libs/jquery/1.7.1/jquery.min.js' );

  if ( is_singular() && get_option( 'thread_comments' ) ){ wp_enqueue_script( 'comment-reply' ); } //Typical threaded comment JS
}

//==Checks enqueued scripts for CDN data
function katana_cdn_scripts($src, $handle) {
	global $wp_scripts;
	$script = $wp_scripts->registered[$handle];
	return (isset($script->extra[cdn])) ? $script->extra[cdn] : $src;
}

if( !is_admin() ){ 
	//==Run Assets function
  add_action( 'wp_enqueue_scripts', 'katana_assets' ); 
  //==Filter CDN sources into script tags
  add_filter('script_loader_src', 'katana_cdn_scripts', 10, 2);
}

//=== Admin Updates
// http://www.deluxeblogtips.com/2011/01/remove-dashboard-widgets-in-wordpress.html
function remove_dashboard_widgets() {
  remove_meta_box('dashboard_incoming_links', 'dashboard', 'normal');
  remove_meta_box('dashboard_plugins', 'dashboard', 'normal');
  remove_meta_box('dashboard_primary', 'dashboard', 'normal');
  remove_meta_box('dashboard_secondary', 'dashboard', 'normal');
}
if ( is_admin() ){
	add_action('admin_init', 'remove_dashboard_widgets');
}
?>